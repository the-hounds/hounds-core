# frozen_string_literal: true

RSpec.describe Hounds::Core do
  it 'has a version number' do
    expect(Hounds::Core::VERSION).not_to be nil
  end
end
